<?php
/* this is not an entry point */
if (!defined("ALCES")) { exit("Not a valid entry point."); }

/*
 * Get list of recommended videoconferences:
 *  1. define default
 *  2. get videoconferences from the config file, of that exists
 * */
$content["vc_list"] = "<a href=\"https://meet.jit.si\">Jitsi</a>";

if (isset($config["videoconferences"])) {
	$vcf_tmp = [];
	foreach ($config["videoconferences"] as $name => $link) {
		$vcf_tmp[] = "<a href=\"$link\">$name</a>";
	}
	$content["vc_list"] = implode(", ", $vcf_tmp);
}

/* home page HTML body */
$content["body"] = <<<CNT_HOME
<p><strong>{$content["home_intro"]}</strong> {$content["home_description"]}
({$content["vc_list"]}).</p>
<p><strong>{$content["home_procedure_intro"]}</strong></p>
<ol>
	<li>{$content["home_procedure_1"]}</li>
	<li>{$content["home_procedure_2"]}</li>
	<li>{$content["home_procedure_3"]}</li>
</ol>
<form>
	<fieldset>
		<legend>{$content["home_form_legend"]}</legend>
		<table>
		<tr>
		<td>{$content["home_form_range"]}</td>
		<td>
			<input type="number" min="1" max="999" value="1" size="3" name="min" required>
			{$content["to"]}
			<input type="number" min="1" max="999" value="100" size="3" name="max" required>
		</td>
		</tr>
		<tr>
		<td>{$content["home_form_count"]}</td>
		<td>
			<input type="number" min="1" max="999" value="1" size="3" name="count" required>
		</td>
		</tr>
		</table>
		<input type="submit" name="new" value="{$content["home_form_create"]}">
	</fieldset>
</form>
CNT_HOME;

/* basic HTML template */
include "template.php";