<?php
/* this is not an entry point */
if (!defined("ALCES")) { exit("Not a valid entry point."); }

/* error page HTML body */
$content["body"] = <<<CNT_ERROR
<p><strong>{$content["error_parameters"]}</strong></p>
<p>{$content["error_check_parameters"]}</p>
CNT_ERROR;

/* basic HTML template */
include "template.php";